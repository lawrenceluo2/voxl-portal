#include "header_manager.h"


#include <voxl_cutils.h>
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional but necessary for battery/gps/flight data
#include <c_library_v2/common/mavlink.h>
#endif

#include <modal_pipe.h>

#include <algorithm>
#include <vector>

#include "manager_utils.h"
#include "page_manager.h"

#define MAVLINK_GCS_PIPE		"mavlink_to_gcs"

#define FLIGHT_INFO_INDEX 0
#define STATUS_TEXT_INDEX 1

extern struct mg_mgr mgr_;

static websocket_info ws_info;

// libmodal_pipe channel
static int channel = -1;

typedef struct flight_msg
{
	uint8_t msg_index;
	uint8_t armed;
	uint8_t num_sats;
	uint8_t main_mode;
	uint32_t sub_mode;
	uint8_t batt_remaining;
} __attribute__((packed)) flight_msg;

// current "state" of the autopilot
static flight_msg curr_msg;




static void _flight_info_connect_cb(int ch, __attribute__((unused)) void *context)
{
	printf("Server providing: flight_info connected\n");
}

static void _flight_info_disconnect_cb(int ch, __attribute__((unused)) void *context)
{
	printf("Server providing: flight_info disconnected\n");
}


static void FlightInfoCallback(int ch, char *data, int bytes, __attribute__((unused)) void *context)
{
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional, most clients won't use it

	int should_send = 0;

	if (!ws_info.connected) return;
	curr_msg.msg_index = FLIGHT_INFO_INDEX;

	// validate that the data makes sense
	int n_packets, i;
	mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
	if(msg_array == NULL){
		pipe_client_flush(ch);
		return;
	}

	// loop through all packets, trimming out useful data
	for(i=0;i<n_packets;i++){

		mavlink_message_t* msg = &msg_array[i];

		// ignore messages not from the autopilot (e.g. discard GCS messages)
		if(msg->compid != MAV_COMP_ID_AUTOPILOT1) continue;

		// handle mavlink error messages
		if (msg->msgid == MAVLINK_MSG_ID_STATUSTEXT){
			char status_text[128];
			char *text = (char *)&_MAV_PAYLOAD(msg)[1]; // pointer to text in the mavlink msg
			memset(status_text, 0, 128);
			status_text[0] = STATUS_TEXT_INDEX;
			snprintf(&status_text[1], 127, "%s", text);

			//printf("Sending: %s\n", text);
			struct mg_connection *c;
			for (c = mgr_.conns; c != NULL; c = c->next){
				if (!(std::count(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id))){
					continue;
				}
				// TODO check if we can send strlen+2
				mg_ws_send(c, status_text, 128, WEBSOCKET_OP_BINARY);
			}
		}
		else if (msg->msgid == MAVLINK_MSG_ID_HEARTBEAT){

			bool armed = (mavlink_msg_heartbeat_get_base_mode(msg) & MAV_MODE_FLAG_SAFETY_ARMED);
			uint8_t main_mode = (mavlink_msg_heartbeat_get_custom_mode(msg) & 0x00FF0000) >> 16;
			uint32_t sub_mode = (mavlink_msg_heartbeat_get_custom_mode(msg) & 0xFF000000) >> 24;

			curr_msg.armed = armed;
			curr_msg.main_mode = main_mode;
			curr_msg.sub_mode = sub_mode;
			should_send = 1;
		}
		else if (msg->msgid == MAVLINK_MSG_ID_GPS_RAW_INT){
			curr_msg.num_sats = mavlink_msg_gps_raw_int_get_satellites_visible(msg);
			should_send = 1;
		}
		else if (msg->msgid == MAVLINK_MSG_ID_SYS_STATUS){
			curr_msg.batt_remaining = mavlink_msg_sys_status_get_battery_remaining(msg);
			should_send = 1;
		}
		else{
			continue;
		}
	}

	if(should_send){
		struct mg_connection *c;
		for (c = mgr_.conns; c != NULL; c = c->next){
			if (!(std::count(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id))){
				continue;
			}
			mg_ws_send(c, (char *)&curr_msg, sizeof(curr_msg), WEBSOCKET_OP_BINARY);
		}
	}
#endif // ENABLE_MAVLINK_SUPPORT
	return;
}

void HeaderManagerCallback(struct mg_connection *c, int ev, void *ev_data, void *fn_data)
{
#ifdef ENABLE_MAVLINK_SUPPORT // mavlink is optional but necessary for battery/gps/flight data

	if (c->is_closing)
	{
		// Remove this connection from our connection ids
		ws_info.connection_ids.erase(std::remove(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id), ws_info.connection_ids.end());

		// Close the pipe if no longer sending to any websockets
		if (ws_info.connection_ids.empty())
		{
			ws_info.connected = false;
			pipe_client_close(channel);
		}
		return;
	}

	if (!ws_info.connected)
	{
		ws_info.connected = true;


		int flight_ch = pipe_client_get_next_available_channel();

		pipe_client_set_connect_cb(flight_ch, _flight_info_connect_cb, NULL);
		pipe_client_set_disconnect_cb(flight_ch, _flight_info_disconnect_cb, NULL);
		pipe_client_set_simple_helper_cb(flight_ch, FlightInfoCallback, c);
		pipe_client_open(flight_ch, MAVLINK_GCS_PIPE, PROCESS_NAME,
							   EN_PIPE_CLIENT_SIMPLE_HELPER, \
							   MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);

		channel = flight_ch;
	}

	ws_info.connection_ids.push_back(c->id);
#endif // ENABLE_MAVLINK_SUPPORT
}
