#include "video_group_manager.h"

#include <modal_pipe.h>
#include <stdlib.h>
#include <voxl_cutils.h>
#include <turbojpeg.h>

#include <algorithm>
#include <vector>
#include <mutex>

#include "cmd_group_manager.h"
#include "manager_utils.h"
#include "page_manager.h"

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#define THROW(action, message)                                                 \
    {                                                                          \
        printf("ERROR in line %d while %s:\n%s\n", __LINE__, action, message); \
    }

#define THROW_TJ(action) THROW(action, tjGetErrorStr2(tjInstance))

#define THROW_UNIX(action) THROW(action, strerror(errno))

#define min(x, y) ((x) > (y) ? (y) : (x))

#define DEFAULT_SUBSAMP TJSAMP_GRAY

#define TIMEOUT_THRES_NS 5000000000L

#define DEBUG_RATE_NS 1000000000L

extern struct mg_mgr mgr_;

static uint64_t running_count = 0;

#define NUM_TIME_SAMPLES 20
#define NUM_QUALITY_STEPS 32

// Start low for better performance!
#define QUALITY_STEP_START 10

uint8_t quality_steps[NUM_QUALITY_STEPS] =
    {
        10, 12, 13, 14, 15, 16, 18, 20,
        22, 25, 28, 31, 34, 37, 40, 43,
        46, 49, 52, 55, 58, 61, 64, 67,
        70, 73, 76, 79, 81, 84, 87, 90};

typedef struct VideoStats
{
    char name[128];
    float fps_sending;
    float fps_actual;
    uint8_t quality = quality_steps[QUALITY_STEP_START];
    char server_name[128] = "unknown";
    int32_t exposure_ns;
    int16_t gain;
    int16_t format;
} __attribute__((packed)) VideoStats;

// Simple LL to keep track of running cameras
// also stores quality for that specific cam
typedef struct _cam_data
{
    int64_t ts = -1;
    VideoStats video_stats;
    int64_t times[NUM_TIME_SAMPLES] = {0};
    bool skipped[NUM_TIME_SAMPLES] = {0};
    int time_index = 0;
    uint8_t quality_step_index = QUALITY_STEP_START;
    uint32_t frame_num = 0;
} _cam_data;

static _cam_data cams[PIPE_CLIENT_MAX_CHANNELS];

static websocket_info ws_info;

std::vector<std::mutex> vid_stats_lock(PIPE_CLIENT_MAX_CHANNELS);
std::mutex websocket_lock;

void _remove_cam_from_list(int ch)
{
    memset(cams[ch].video_stats.name, 0, sizeof(cams[ch].video_stats.name));
    cams[ch].ts = -1;
    cams[ch].video_stats.quality = quality_steps[QUALITY_STEP_START];
    cams[ch].frame_num = 0;
    cams[ch].quality_step_index = QUALITY_STEP_START;
}

static int _get_cam_channel(const char *name)
{
    for (int i = 0; i < PIPE_CLIENT_MAX_CHANNELS; i++)
    {
        if (cams[i].ts != -1 && !strcmp(cams[i].video_stats.name, name))
            return i;
    }
    return -1;
}

static pthread_mutex_t yuv_buffer_mutex = PTHREAD_MUTEX_INITIALIZER;
static unsigned char *yuv_buffer;
static unsigned int yuv_buffer_size = 0;

// Converts a nv12 image to YUV420
static void _cvt_nv_yuv(unsigned char *frame, int width, int height, int isNV12)
{
    int i, j;
    int y_size = width * height;

    pthread_mutex_lock(&yuv_buffer_mutex);

    if (yuv_buffer_size < y_size / 2) {
        if (yuv_buffer_size == 0) {
            yuv_buffer = (unsigned char *)malloc(y_size/2);
        } else {
            yuv_buffer = (unsigned char *)realloc(yuv_buffer, y_size/2);
        }
        yuv_buffer_size = y_size / 2;
    }

    if (!yuv_buffer) {
        printf("ERROR: Failed to malloc YUV buffer for resampling\n");
        pthread_mutex_unlock(&yuv_buffer_mutex);
        exit(-1);
    }

    memcpy(yuv_buffer, frame + y_size, (y_size / 2));

    unsigned char *u = frame + y_size;
    unsigned char *v = frame + (y_size * 5 / 4);

    for (int i = 0; i < y_size / 4; i++)
    {
        if (isNV12)
        { // nv12
            u[i] = yuv_buffer[(i * 2)];
            v[i] = yuv_buffer[(i * 2) + 1];
        }
        else
        { // nv21
            u[i] = yuv_buffer[(i * 2) + 1];
            v[i] = yuv_buffer[(i * 2)];
        }
    }

    pthread_mutex_unlock(&yuv_buffer_mutex);
}

static void _make_422_planar(unsigned char *frame, int width, int height)
{
    int i, j;
    int size = width * height * 2;

    pthread_mutex_lock(&yuv_buffer_mutex);

    if (yuv_buffer_size < size) {
        if (yuv_buffer_size == 0) {
            yuv_buffer = (unsigned char *)malloc(size);
        } else {
            yuv_buffer = (unsigned char *)realloc(yuv_buffer, size);
        }
        yuv_buffer_size = size;
    }

    if (!yuv_buffer) {
        printf("ERROR: Failed to malloc YUV buffer for resampling\n");
        pthread_mutex_unlock(&yuv_buffer_mutex);
        exit(-1);
    }

    memcpy(yuv_buffer, frame, size);

    unsigned char *u = frame + (size / 2);
    unsigned char *v = frame + (size * 3 / 4);

    for (int i = 0; i < size / 4; i++)
    {
        frame[i*2]     = yuv_buffer[i*4];
        frame[i*2 + 1] = yuv_buffer[i*4 + 2];
        u[i]           = yuv_buffer[i*4 + 1];
        v[i]           = yuv_buffer[i*4 + 3];
    }

    pthread_mutex_unlock(&yuv_buffer_mutex);
}

static __attribute__((destructor)) void cleanupbuffer()
{
    if (yuv_buffer) free(yuv_buffer);
}

// Calculate the average framerate from the last NUM_TIME_SAMPLES frames
static void calc_avg_fr_hz(_cam_data &cam)
{
    int64_t max_full_time = -1;
    int64_t min_full_time = 0x7FFFFFFFFFFFFFFF;

    int64_t full_interval;

    uint8_t skip_count = 0;

    // find min and max times
    for (int i = 0; i < NUM_TIME_SAMPLES; i++)
    {
        if (cam.skipped[i]){
            skip_count++;
        }
        if (cam.times[i] < min_full_time){
            min_full_time = cam.times[i];
        }
        if (cam.times[i] > max_full_time)
        {
            max_full_time = cam.times[i];
        }
    }

    // calculate duration
    full_interval = max_full_time - min_full_time;

    // number of frames / number of seconds
    cam.video_stats.fps_actual =  (NUM_TIME_SAMPLES - 1) / (double)(full_interval / 1000000000.0);
    cam.video_stats.fps_sending = (NUM_TIME_SAMPLES - skip_count - 1) / (double)(full_interval / 1000000000.0);
}

#define FMT_INVALID 0
#define FMT_STANDARD 1
#define FMT_YUV 2
#define FMT_NV 3
#define FMT_STEREO_NV 4
static int tj_fmt_from_mpa(camera_image_metadata_t meta,
                           int *infmt, int *outfmt)
{
    switch (meta.format)
    {
    case IMAGE_FORMAT_RAW8:
    case IMAGE_FORMAT_STEREO_RAW8:
        *infmt = TJPF_GRAY;
        *outfmt = TJSAMP_GRAY;
        return FMT_STANDARD;

    case IMAGE_FORMAT_RGB:
        *infmt = TJPF_RGB;
        *outfmt = TJSAMP_422;
        return FMT_STANDARD;

    case IMAGE_FORMAT_YUV420:
        *outfmt = TJSAMP_420;
        return FMT_YUV;

    case IMAGE_FORMAT_YUV422:
        *outfmt = TJSAMP_422;
        return FMT_YUV;

    case IMAGE_FORMAT_NV21:
    case IMAGE_FORMAT_NV12:
        *outfmt = TJSAMP_420;
        return FMT_NV;

    case IMAGE_FORMAT_STEREO_NV12:
    case IMAGE_FORMAT_STEREO_NV21:
        *outfmt = TJSAMP_420;
        return FMT_STEREO_NV;

    /*
    case IMAGE_FORMAT_FLOAT32 :
    case IMAGE_FORMAT_RAW16 :
    case IMAGE_FORMAT_H264 :
    case IMAGE_FORMAT_H265 :
    case IMAGE_FORMAT_JPG :*/
    default:
        return FMT_INVALID; // unsupported
    }
}

bool send_vid_stats()
{
    std::lock_guard<std::mutex> lg(websocket_lock);
    static int64_t last_call_t_ns = VCU_time_monotonic_ns();

    if (VCU_time_monotonic_ns() - last_call_t_ns < DEBUG_RATE_NS) return false;

    char *vid_stats_ptr = nullptr;

    if (!ws_info.connected)
        return false;

    int16_t cam_count = 0;
    for (int i = 0; i < PIPE_CLIENT_MAX_CHANNELS; i++)
    {
        if (cams[i].ts != -1)
            cam_count++;
    }

    if (cam_count <= 0)
        return false;

    vid_stats_ptr = (char *)malloc(sizeof(VideoStats) * cam_count + sizeof(int16_t));
    memcpy(vid_stats_ptr, &cam_count, sizeof(int16_t));
    int offset = sizeof(int16_t);
    for (int i = 0; i < PIPE_CLIENT_MAX_CHANNELS; i++)
    {
        if (cams[i].ts != -1)
        {
            vid_stats_lock[i].lock();
            memcpy(vid_stats_ptr + offset, &(cams[i].video_stats), sizeof(VideoStats));
            offset += sizeof(VideoStats);
            vid_stats_lock[i].unlock();
        }
    }

    struct mg_connection *c;
    for (c = mgr_.conns; c != NULL; c = c->next)
    {
        if (!(std::count(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id)))
            continue;

        if (c->send.len > offset/2)
        {
            // fprintf(stderr, "dropping vid stats packet\n");
            continue;
        }
        mg_ws_send(c, vid_stats_ptr, offset, WEBSOCKET_OP_BINARY);
    }

    free(vid_stats_ptr);
    vid_stats_ptr = nullptr;

    return true;
}

static void CameraDataCallback(int ch, camera_image_metadata_t meta, char *frame, void *context)
{
    // We can't process the frames fast enough, the pipe is getting backed up, skip a frame
    if (pipe_client_bytes_in_pipe(ch))
    {
        cams[ch].skipped[cams[ch].time_index] = true;
        cams[ch].times[cams[ch].time_index++] = meta.timestamp_ns;
        cams[ch].time_index %= NUM_TIME_SAMPLES;
        cams[ch].quality_step_index = MAX(cams[ch].quality_step_index - 3, 0);
        cams[ch].ts = meta.timestamp_ns;
        // fprintf(stderr, "dropping frame\n");
        return;
    }
    else if (cams[ch].frame_num % 4 == 0)
    {
        cams[ch].quality_step_index = MIN(cams[ch].quality_step_index + 1, NUM_QUALITY_STEPS - 1);
    }

    cams[ch].video_stats.quality = quality_steps[cams[ch].quality_step_index];
    cams[ch].skipped[cams[ch].time_index] = false;
    cams[ch].times[cams[ch].time_index++] = meta.timestamp_ns;
    cams[ch].time_index %= NUM_TIME_SAMPLES;
    cams[ch].video_stats.format = meta.format;
    cams[ch].video_stats.gain = meta.gain;
    cams[ch].video_stats.format = meta.format;
    cams[ch].video_stats.exposure_ns = meta.exposure_ns/1000000.;

    vid_stats_lock[ch].lock();
    calc_avg_fr_hz(cams[ch]);
    vid_stats_lock[ch].unlock();

    static pipe_info_t cam_info;
    if (strcmp(cams[ch].video_stats.server_name, cam_info.server_name))
    {
        pipe_client_get_info(ch, &cam_info);
        memcpy(cams[ch].video_stats.server_name, &(cam_info.server_name), sizeof(cams[ch].video_stats.server_name));
    }

    int64_t curTime = VCU_time_monotonic_ns();

    if (curTime - cams[ch].ts > TIMEOUT_THRES_NS)
    {
        fprintf(stderr, "Ceasing to publish %s at %ld\n", cams[ch].video_stats.name, curTime);
        _remove_cam_from_list(ch);
        pipe_client_close(ch);
        return;
    }

    /* BEGIN JPEG COMPRESS */
    tjhandle tjInstance = NULL;
    unsigned long jpegSize = 0;
    unsigned char *jpegBuf = nullptr; /* Dynamically allocate the JPEG buffer */
    int pixelFormat;
    int outSubsamp;
    int outQual = cams[ch].video_stats.quality;
    int flags = 0;
    // speed up flags
    flags |= TJFLAG_FASTUPSAMPLE;
    flags |= TJFLAG_FASTDCT;
    int width = meta.width;
    int height = meta.height * (meta.format == IMAGE_FORMAT_STEREO_RAW8 ? 2 : 1);

    switch (tj_fmt_from_mpa(meta, &pixelFormat, &outSubsamp))
    {
    case FMT_STANDARD:

        if ((tjInstance = tjInitCompress()) == NULL)
            THROW_TJ("initializing compressor");

        if (tjCompress2(tjInstance, (uint8_t *)frame, width, 0, height, pixelFormat,
                        &jpegBuf, &jpegSize, outSubsamp, outQual, flags) < 0)
            THROW_TJ("compressing image");

        tjDestroy(tjInstance);
        tjInstance = NULL;
        break;

    case FMT_STEREO_NV:
    {
        // special case: have two stacked nv frames
        // need to move all the color channels to the end of the image n1, v1, n2, v2
        // grab the nv channels of first image
        unsigned char *nv_mover = (unsigned char *)malloc(width * height / 2);
        memcpy(nv_mover, frame + (width * height), width * height / 2);

        // now that we have them, we can overwrite that section of the image with the next full black/white frame
        memcpy(frame + (width * height), frame + (width * height * 3 / 2), width * height);

        // throw frame 1 nv channels at the end of image 2
        memcpy(frame + (width * height * 2), nv_mover, width * height / 2);

        free(nv_mover);
        nv_mover = nullptr;

        _cvt_nv_yuv((unsigned char *)frame, width, height * 2, meta.format == IMAGE_FORMAT_STEREO_NV12);

        // good to go now with the double nv frame?
        if ((tjInstance = tjInitCompress()) == NULL)
            THROW_TJ("initializing compressor");

        if (tjCompressFromYUV(tjInstance, (uint8_t *)frame, width, 1, height * 2, outSubsamp,
                              &jpegBuf, &jpegSize, outQual, flags) < 0)
            THROW_TJ("compressing image");

        tjDestroy(tjInstance);
        tjInstance = NULL;
    }
    break;

    case FMT_NV:

        _cvt_nv_yuv((unsigned char *)frame, width, height, meta.format == IMAGE_FORMAT_NV12);

    case FMT_YUV:

        if ((tjInstance = tjInitCompress()) == NULL)
            THROW_TJ("initializing compressor");

        if (outSubsamp == TJSAMP_422) _make_422_planar((uint8_t *)frame, width, height);

        if (tjCompressFromYUV(tjInstance, (uint8_t *)frame, width, 1, height, outSubsamp,
                              &jpegBuf, &jpegSize, outQual, flags) < 0)
            THROW_TJ("compressing image");

        tjDestroy(tjInstance);
        tjInstance = NULL;

        break;

    case FMT_INVALID:
    default:
        return;
    }

    if (jpegBuf == nullptr)
        return;

    cams[ch].frame_num++;
    int i = 0;
    for (struct mg_connection *c = mgr_.conns; c != NULL; c = c->next)
    {
        if (strcmp(c->label, cams[ch].video_stats.name) != 0)
        {
            continue;
        }

        // This can be the start of quality and framerate control
        // There is some funny business with memory management in
        // the mg_send function. Seems to be trying to minimize
        // memory usage which is definitely not an issue here.
        // The following code writes to the socket directly
        // and seems to be significantly more stable.
        char header[256];
        char footer[256];
        int header_len;
        // no footer needed due to initial multi-part def
        header_len = snprintf(header, sizeof(header),
                              "--foo\r\nContent-Type: image/jpeg\r\n"
                              "Content-Length: %lu\r\n\r\n",
                              (unsigned long)jpegSize);

        char *message = (char *)malloc(header_len + jpegSize);
        memcpy(message, header, header_len);
        memcpy(message + header_len, jpegBuf, jpegSize);

        send((int64_t)c->fd, message, header_len + jpegSize, 0);

        free(message);

        cams[ch].ts = curTime;
    }

    tjFree(jpegBuf);

    if (running_count % 10 == 0){
        send_vid_stats();
    }

    running_count++;
    return;
}

static void _connect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: %s connected\n", cams[ch].video_stats.name);
    // Reset timeout timer since we've reconnected to the server
    cams[ch].ts = VCU_time_monotonic_ns();
}

static void _disconnect_cb(int ch, __attribute__((unused)) void *context)
{
    printf("Server providing: %s disconnected\n", cams[ch].video_stats.name);
}

void VideoGroupManagerCallback(struct mg_connection *c, int ev,
                               void *ev_data, void *fn_data)
{
    struct mg_http_message *hm = (struct mg_http_message *)ev_data;

    // Chop off the /video/ of the uri to get the camera name
    const char *tmp = &(hm->uri.ptr[strlen(VideoGroupManagerClaimString) - 1]);

    // And remove all of the https garbage, we just want the pipe name
    int len = min(strchr(tmp, ' ') - tmp, strchr(tmp, '/') - tmp);

    char in_name[64];

    // Copy name in and terminate the string
    strncpy(in_name, tmp, len);
    in_name[len] = 0;

    int j = 0;
    for (int i = 0; in_name[i] != 0; i++)
    {
        if (valid_char(in_name[i]))
            in_name[j++] = in_name[i];
    }
    in_name[j] = 0;

    printf("Recieved Video request for: %s\n", in_name);

    int ch = _get_cam_channel(in_name);

    if (ch == -1)
    {
        char pipe_name[256];
        ch = pipe_client_get_next_available_channel();

        if (ch == -1)
            return;

        strcpy(cams[ch].video_stats.name, in_name);
        strcpy(pipe_name, in_name);
        cams[ch].ts = VCU_time_monotonic_ns();

        pipe_client_set_connect_cb(ch, _connect_cb, NULL);
        pipe_client_set_disconnect_cb(ch, _disconnect_cb, NULL);
        pipe_client_set_camera_helper_cb(ch, CameraDataCallback, NULL);
        if (int ret = pipe_client_open(ch, pipe_name, PROCESS_NAME,
                                       CLIENT_FLAG_EN_CAMERA_HELPER, 0))
        {
            pipe_print_error(ret);
            fprintf(stderr, "\n\nFailed to open pipe: %s\n\n\n", in_name);
            _remove_cam_from_list(ch);
        }
    }

    cams[ch].ts = VCU_time_monotonic_ns();

    strcpy(c->label, in_name);

    mg_printf(
        c, "%s",
        (char *)"HTTP/1.0 200 OK\r\n"
                "Cache-Control: no-cache\r\n"
                "Pragma: no-cache\r\nExpires: Thu, 01 Dec 1994 16:00:00 GMT\r\n"
                "Content-Type: multipart/x-mixed-replace; boundary=--foo\r\n\r\n");
}

void VideoStatsManagerCallback(struct mg_connection *c, int ev, void *ev_data, void *fn_data)
{
    if (c->is_closing)
    {
        // Remove this connection from our connection ids
        ws_info.connection_ids.erase(std::remove(ws_info.connection_ids.begin(), ws_info.connection_ids.end(), c->id), ws_info.connection_ids.end());
    }
    else
    {
        ws_info.connection_ids.push_back(c->id);
        printf("recieved video stats callback\n");
        ws_info.connected = true;
    }
}
