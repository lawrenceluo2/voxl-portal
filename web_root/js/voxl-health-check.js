
var term = new Terminal({
	fontSize: 14,
    fontFamily: 'Ubuntu Mono, courier-new, courier, monospace'
});
term.open(document.getElementById('voxl-health-check'),false);
term.resize(100, 100); 

const tests = ["api/v1/platform/test"]

writeHeader = function() {
	term.writeln([
		'VOXL Health Check',
		''
	].join('\n\r'));
}

healthCheckWriteln = function(str){
	term.writeln(str);
}
healthCheckWritelnPass = function(str){
	term.writeln('\x1b[32m' + str + '\x1b[0m');
}
healthCheckWritelnFail = function(str){
	term.writeln('\x1b[31;1m' + str + '\x1b[0m');
}

healthCheckVoxlPx4 = function(data){
	healthCheckWriteln(' < SDK: voxl-px4: ')
	
	var voxlPx4 = data['voxl-px4'];

	if(voxlPx4){
		if(voxlPx4["error"] == true){
			healthCheckWritelnFail('ERROR: voxl-px4: likely not running?')
		}
		else {
			if(voxlPx4['sensor_accel']){
				var sensor = voxlPx4['sensor_accel']
				var result = sensor["result"]
				if(result == true) {
					healthCheckWritelnPass('           : accel x,y,z: ' + sensor['accel_x'] + ', ' + sensor['accel_y'] + ',  ' + sensor['accel_z'])
				} else {
					healthCheckWritelnFail('           : accel x,y,z: ' + sensor['accel_x'] + ', ' + sensor['accel_y'] + ',  ' + sensor['accel_z'])
				}
			}
			if(voxlPx4['sensor_gyro']){
				var sensor = voxlPx4['sensor_gyro']
				var result = sensor["result"]
				if(result == true) {
					healthCheckWritelnPass('           : gyro x,y,z ' + sensor['gyro_x'] + ', ' + sensor['gyro_y'] + ', ' + sensor['gyro_z'])
				} else {
					healthCheckWritelnFail('           : gyro x,y,z ' + sensor['gyro_x'] + ', ' + sensor['gyro_y'] + ', ' + sensor['gyro_z'])
				}
			}
			if(voxlPx4['sensor_baro']){
				var sensor = voxlPx4['sensor_baro']
				var result = sensor["result"]
				if(result == true) {
					healthCheckWritelnPass('           : baro temp,pres ' + sensor['temperature'] + ', ' + sensor['pressure'])
				} else {
					healthCheckWritelnFail('           : baro temp,pres ' + sensor['temperature'] + ', ' + sensor['pressure'])
				}
			}
			if(voxlPx4['sensor_mag']){
				var sensor = voxlPx4['sensor_mag']
				var result = sensor["result"]
				if(result == true) {
					healthCheckWritelnPass('           : mag x,y,z ' + sensor['mag_x'] + ', ' + sensor['mag_y'] + ', ' + sensor['mag_z'])
				} else {
					healthCheckWritelnFail('           : mag x,y,z ' + sensor['mag_x'] + ', ' + sensor['mag_y'] + ', ' + sensor['mag_z'])
				}
			}
			if(voxlPx4['sensor_gps']){
				var sensor = voxlPx4['sensor_gps']
				var result = sensor["result"]
				if(result == true) {
					healthCheckWritelnPass('           : sats: ' + sensor['sats'])
				} else {
					healthCheckWritelnFail('           : sats: ' + sensor['sats'])
				}
			}
			if(voxlPx4['battery_status']){
				var sensor = voxlPx4['battery_status']
				var result = sensor["result"]
				if(result == true) {
					healthCheckWritelnPass('           : voltage: ' + sensor['voltage'])
				} else {
					healthCheckWritelnFail('           : voltage: ' + sensor['voltage'])
				}
			}
			if(voxlPx4['input_rc']){
				var sensor = voxlPx4['input_rc']
				var result = sensor["result"]
				if(result == true) {
					healthCheckWritelnPass('           : chancount: ' + sensor['chancount'])
				} else {
					healthCheckWritelnFail('           : chancount: ' + sensor['chancount'])
				}
			}
		}
	} else {
		healthCheckWritelnFail('ERROR: failed to print voxl-px4 status')
	}
}


healthCheckPrintResultsD0005 = function(data){

	healthCheckWriteln('<< SDK: factoryMode: ' + data['factoryMode'])

	healthCheckWriteln("")
	var compute = data['compute'];

	// voxl-camera-server - show low level data
	healthCheckWriteln('---- voxl-camera-server-----------------------------------')
	if(compute){
		// D0005-V2
		healthCheckWriteln(' < platform: imageSensor')
		// 0
		var result0 = data['compute']['imageSensor0']['result']
		if(result0 == true){
			healthCheckWritelnPass('     imageSensor0: ' + compute['imageSensor0']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			healthCheckWritelnFail('     imageSensor0: ' + compute['imageSensor0']['probe'])
		}
		// 1
		var result1 = data['compute']['imageSensor1']['result']
		if(result1 == true){
			healthCheckWritelnPass('     imageSensor1: ' + compute['imageSensor1']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			WritelnFail('     imageSensor1: ' + compute['imageSensor1']['probe'])
		}
		// 2
		var result1 = data['compute']['imageSensor2']['result']
		if(result1 == true){
			healthCheckWritelnPass('     imageSensor2: ' + compute['imageSensor2']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			WritelnFail('     imageSensor2: ' + compute['imageSensor2']['probe'])
		}
	} else {
		healthCheckWritelnFail('D0005 FATAL ERROR: unable to get compute info')
	}

	// voxl-camera-server - show high level data
	healthCheckWriteln("")
	var voxlCameraServer = data['voxl-camera-server'];
	if(voxlCameraServer){
		healthCheckWriteln(' < SDK: voxl-camera-server: ')

		var result = data['voxl-camera-server']['result']
		if(result){
			healthCheckWritelnPass('           : OK')
		} else {
			healthCheckWritelnFail('           : Error')
		}

		healthCheckWriteln('   MPA ')
		var result = data['voxl-camera-server']['mpaOk']
		if(result){
			healthCheckWritelnPass('           : mpaTofPc: ' + voxlCameraServer['mpaTofPc'])
			healthCheckWritelnPass('           : mpaTofIr: ' + voxlCameraServer['mpaTofIr'])
			healthCheckWritelnPass('           : mpaTofConf: ' + voxlCameraServer['mpaTofConf'])
			healthCheckWritelnPass('           : mpaTofDepth: ' + voxlCameraServer['mpaTofDepth'])
			healthCheckWritelnPass('           : mpaTracking: ' + voxlCameraServer['mpaTracking'])
			healthCheckWritelnPass('           : mpaHires: ' + voxlCameraServer['mpaHires'])
		} else {
			healthCheckWritelnFail('           : mpaTofPc: ' + voxlCameraServer['mpaTofPc'])
			healthCheckWritelnFail('           : mpaTofIr: ' + voxlCameraServer['mpaTofIr'])
			healthCheckWritelnFail('           : mpaTofConf: ' + voxlCameraServer['mpaTofConf'])
			healthCheckWritelnFail('           : mpaTofDepth: ' + voxlCameraServer['mpaTofDepth'])
			healthCheckWritelnFail('           : mpaTracking: ' + voxlCameraServer['mpaTracking'])
			healthCheckWritelnFail('           : mpaHires: ' + voxlCameraServer['mpaHires'])
		}
		healthCheckWriteln('   Calibration ')
		var result = data['voxl-camera-server']['calOk']
		if(result){
			healthCheckWritelnPass('           : cal_a: ' + voxlCameraServer['cal_a'])
		} else {
			healthCheckWritelnFail('           : cal_a: ' + voxlCameraServer['cal_a'])
		}
	}
	else {
		healthCheckWritelnFail('D0005 FATAL ERROR: unable to get voxl-camera-server info')
	}

	healthCheckWriteln("")
	healthCheckWriteln('---- voxl-px4 ---------------------------------------------')
	healthCheckVoxlPx4(data)

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-vision-hub --------------------------------------')

	// show high level
	healthCheckWriteln(' < SDK: voxl-vision-hub: ')
	var voxlVisionHub = data['voxl-vision-hub'];
	if(voxlVisionHub){
		if(voxlVisionHub['result']){
			healthCheckWritelnPass('           : running: ' + voxlVisionHub['running'])
			//healthCheckWritelnPass('           : mpaPipe: ' + voxlVisionHub['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlVisionHub['running'])
			//healthCheckWritelnFail('           : mpaPipe: ' + voxlVisionHub['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-vision-hub info')
	}

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-qvio-server --------------------------------------')

	// show high level
	healthCheckWriteln(' < SDK: voxl-qvio-server: ')
	var voxlQvioServer = data['voxl-qvio-server'];
	if(voxlQvioServer){
		if(voxlQvioServer['result']){
			healthCheckWritelnPass('           : running: ' + voxlQvioServer['running'])
			healthCheckWritelnPass('           : mpaPipe: ' + voxlQvioServer['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlQvioServer['running'])
			healthCheckWritelnFail('           : mpaPipe: ' + voxlQvioServer['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-qvio-server info')
	}

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-mavlink-server --------------------------------------')

	// show high level
	healthCheckWriteln(' < SDK: voxl-mavlink-server: ')
	var voxlMavlinkServer = data['voxl-mavlink-server'];
	if(voxlMavlinkServer){
		if(voxlMavlinkServer['result']){
			healthCheckWritelnPass('           : running: ' + voxlMavlinkServer['running'])
			//healthCheckWritelnPass('           : mpaPipe: ' + voxlMavlinkServer['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlMavlinkServer['running'])
			//healthCheckWritelnFail('           : mpaPipe: ' + voxlMavlinkServer['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-mavlink-server info')
	}
}


healthCheckPrintResultsD0006 = function(data){

	healthCheckWriteln('<< SDK: factoryMode: ' + data['factoryMode'])

	healthCheckWriteln("")
	var compute = data['compute'];
	
	// voxl-camera-server - show low level data
	healthCheckWriteln('---- voxl-camera-server-----------------------------------')
	if(compute){
		healthCheckWriteln(' < platform: imageSensor')
		// 0
		var result0 = data['compute']['imageSensor0']['result']
		if(result0 == true){
			healthCheckWritelnPass('     imageSensor0: ' + compute['imageSensor0']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			healthCheckWritelnFail('     imageSensor0: ' + compute['imageSensor0']['probe'])
		}
		// 1
		var result1 = data['compute']['imageSensor1']['result']
		if(result1 == true){
			healthCheckWritelnPass('     imageSensor1: ' + compute['imageSensor1']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			WritelnFail('     imageSensor1: ' + compute['imageSensor1']['probe'])
		}
		// 2
		var result1 = data['compute']['imageSensor2']['result']
		if(result1 == true){
			healthCheckWritelnPass('     imageSensor2: ' + compute['imageSensor2']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			WritelnFail('     imageSensor2: ' + compute['imageSensor2']['probe'])
		}
		// 3
		var result1 = data['compute']['imageSensor3']['result']
		if(result1 == true){
			healthCheckWritelnPass('     imageSensor3: ' + compute['imageSensor3']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			WritelnFail('     imageSensor3: ' + compute['imageSensor3']['probe'])
		}
		// 4
		var result1 = data['compute']['imageSensor4']['result']
		if(result1 == true){
			healthCheckWritelnPass('     imageSensor4: ' + compute['imageSensor4']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			WritelnFail('     imageSensor4: ' + compute['imageSensor4']['probe'])
		}
		// 5
		var result1 = data['compute']['imageSensor5']['result']
		if(result1 == true){
			healthCheckWritelnPass('     imageSensor5: ' + compute['imageSensor5']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			WritelnFail('     imageSensor5: ' + compute['imageSensor5']['probe'])
		}

	} else {
		healthCheckWritelnFail('FATAL ERROR: unable to get compute info')
	}

	// voxl-camera-server - show high level data
	healthCheckWriteln("")
	var voxlCameraServer = data['voxl-camera-server'];
	if(voxlCameraServer){
		healthCheckWriteln(' < SDK: voxl-camera-server: ')

		var result = data['voxl-camera-server']['result']
		if(result){
			healthCheckWritelnPass('           : OK')
		} else {
			healthCheckWritelnFail('           : Error')
		}

		healthCheckWriteln('   MPA ')
		var result = data['voxl-camera-server']['mpaOk']
		if(result){
			healthCheckWritelnPass('           : mpaStereoFront: ' + voxlCameraServer['mpaStereoFront'])
			healthCheckWritelnPass('           : mpaStereoRear: ' + voxlCameraServer['mpaStereoRear'])
			healthCheckWritelnPass('           : mpaTracking: ' + voxlCameraServer['mpaTracking'])
			healthCheckWritelnPass('           : mpaHires: ' + voxlCameraServer['mpaHires'])
		} else {
			healthCheckWritelnFail('           : mpaStereoFront: ' + voxlCameraServer['mpaStereoFront'])
			healthCheckWritelnFail('           : mpaStereoRear: ' + voxlCameraServer['mpaStereoRear'])
			healthCheckWritelnFail('           : mpaTracking: ' + voxlCameraServer['mpaTracking'])
			healthCheckWritelnFail('           : mpaHires: ' + voxlCameraServer['mpaHires'])
		}
		healthCheckWriteln('   Calibration ')
		var result = data['voxl-camera-server']['calOk']
		if(result){
			healthCheckWritelnPass('           : cal_a: ' + voxlCameraServer['cal_a'])
			healthCheckWritelnPass('           : cal_b: ' + voxlCameraServer['cal_b'])
			healthCheckWritelnPass('           : cal_c: ' + voxlCameraServer['cal_c'])
			healthCheckWritelnPass('           : cal_d: ' + voxlCameraServer['cal_d'])
			healthCheckWritelnPass('           : cal_e: ' + voxlCameraServer['cal_e'])
		} else {
			healthCheckWritelnFail('           : cal_a: ' + voxlCameraServer['cal_a'])
			healthCheckWritelnFail('           : cal_b: ' + voxlCameraServer['cal_b'])
			healthCheckWritelnFail('           : cal_c: ' + voxlCameraServer['cal_c'])
			healthCheckWritelnFail('           : cal_d: ' + voxlCameraServer['cal_d'])
			healthCheckWritelnFail('           : cal_e: ' + voxlCameraServer['cal_e'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-camera-server info')
	}


	healthCheckWriteln("")
	healthCheckWriteln('---- voxl-px4 ---------------------------------------------')
	healthCheckVoxlPx4(data)

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-vision-px4 --------------------------------------')

	// show high level
	healthCheckWriteln(' < SDK: voxl-vision-px4: ')
	var voxlVisionPx4 = data['voxl-vision-px4'];
	if(voxlVisionPx4){
		if(voxlVisionPx4['result']){
			healthCheckWritelnPass('           : running: ' + voxlVisionPx4['running'])
			//healthCheckWritelnPass('           : mpaPipe: ' + voxlVisionPx4['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlVisionPx4['running'])
			//healthCheckWritelnFail('           : mpaPipe: ' + voxlVisionPx4['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-vision-px4 info')
	}

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-qvio-server --------------------------------------')

	// show high level
	healthCheckWriteln(' < SDK: voxl-qvio-server: ')
	var voxlQvioServer = data['voxl-qvio-server'];
	if(voxlQvioServer){
		if(voxlQvioServer['result']){
			healthCheckWritelnPass('           : running: ' + voxlQvioServer['running'])
			healthCheckWritelnPass('           : mpaPipe: ' + voxlQvioServer['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlQvioServer['running'])
			healthCheckWritelnFail('           : mpaPipe: ' + voxlQvioServer['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-qvio-server info')
	}

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-mavlink-server --------------------------------------')

	// show high level
	healthCheckWriteln(' < SDK: voxl-mavlink-server: ')
	var voxlMavlinkServer = data['voxl-mavlink-server'];
	if(voxlMavlinkServer){
		if(voxlMavlinkServer['result']){
			healthCheckWritelnPass('           : running: ' + voxlMavlinkServer['running'])
			//healthCheckWritelnPass('           : mpaPipe: ' + voxlMavlinkServer['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlMavlinkServer['running'])
			//healthCheckWritelnFail('           : mpaPipe: ' + voxlMavlinkServer['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-mavlink-server info')
	}
}

healthCheckPrintResultsD0008 = function(data){

	healthCheckWriteln('<< SDK: factoryMode: ' + data['factoryMode'])
	//var result = JSON.stringify(data);
	//healthCheckWriteln(result);

	healthCheckWriteln("")
	var compute = data['compute'];
	
	// show low level data
	healthCheckWriteln('---- voxl-camera-server-----------------------------------')
	if(compute){
		healthCheckWriteln(' < platform: imageSensor')
		var result0 = data['compute']['imageSensor0']['result']
		if(result0 == true){
			healthCheckWritelnPass('     imageSensor0: ' + compute['imageSensor0']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			healthCheckWritelnFail('     imageSensor0: ' + compute['imageSensor0']['probe'])
		}
		var result1 = data['compute']['imageSensor1']['result']
		if(result1 == true){
			healthCheckWritelnPass('     imageSensor1: ' + compute['imageSensor1']['probe'])
		} else {
			healthCheckWritelnFail(' image sensor failure')
			healthCheckWritelnFail('     imageSensor1: ' + compute['imageSensor1']['probe'])
		}
	} else {
		healthCheckWritelnFail('FATAL ERROR: unable to get compute info')
	}

	// show high level data
	healthCheckWriteln("")
	var voxlCameraServer = data['voxl-camera-server'];
	if(voxlCameraServer){
		healthCheckWriteln(' < SDK: voxl-camera-server: ')
		var result = data['voxl-camera-server']['result']
		if(result){
			healthCheckWritelnPass('           : mpaStereo: ' + voxlCameraServer['mpaStereo'])
		} else {
			healthCheckWritelnFail('           : mpaStereo: ' + voxlCameraServer['mpaStereo'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-camera-server info')
	}

	healthCheckWriteln("")
	healthCheckWriteln('---- voxl-px4 ---------------------------------------------')
	healthCheckVoxlPx4(data)

	healthCheckWriteln("")
	healthCheckWriteln('---- voxl-lepton-server -----------------------------------')
	// show low level
	if(compute){
		healthCheckWriteln(' < platform: compute: i2c0')
		var i2c0 = data['compute']['i2c0']
		if(i2c0){
			if(i2c0["result"] == true){
				healthCheckWritelnPass('            probe: ' + i2c0["probe"])
			} else {
				healthCheckWritelnFail(' failed to probe device on i2c0')
			}
		}
	} else {
		healthCheckWritelnFail('FATAL ERROR: unable to get compute i2c0 info')
	}

	// show high level
	healthCheckWriteln("")
	healthCheckWriteln(' < SDK: voxl-lepton-server: ')
	var voxlFlirServer = data['voxl-lepton-server'];
	if(voxlFlirServer){
		var result = voxlFlirServer['result']
		if(result){
			healthCheckWritelnPass('           : running: ' + voxlFlirServer['running'])
			healthCheckWritelnPass('           : intrinsics: ' + voxlFlirServer['intrinsics'])
			healthCheckWritelnPass('           : mpaColor: ' + voxlFlirServer['mpaColor'])
			healthCheckWritelnPass('           : mpaRaw: ' + voxlFlirServer['mpaRaw'])
		}
		else {
			healthCheckWritelnFail('ERROR: voxl-lepton-server')
			healthCheckWritelnFail('           : intrinsics: ' + voxlFlirServer['intrinsics'])
			healthCheckWritelnFail('           : mpaColor: ' + voxlFlirServer['mpaColor'])
			healthCheckWritelnFail('           : mpaRaw: ' + voxlFlirServer['mpaRaw'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-lepton-server info')
	}

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-mavlink-server --------------------------------------')

	// show high level
	healthCheckWriteln(' < SDK: voxl-mavlink-server: ')
	var voxlMavlinkServer = data['voxl-mavlink-server'];
	if(voxlMavlinkServer){
		if(voxlMavlinkServer['result']){
			healthCheckWritelnPass('           : running: ' + voxlMavlinkServer['running'])
			//healthCheckWritelnPass('           : mpaPipe: ' + voxlMavlinkServer['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlMavlinkServer['running'])
			//healthCheckWritelnFail('           : mpaPipe: ' + voxlMavlinkServer['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-mavlink-server info')
	}

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-vision-hub --------------------------------------')

	// show high level
	healthCheckWriteln(' < SDK: voxl-vision-hub: ')
	var voxlVisionHub = data['voxl-vision-hub'];
	if(voxlVisionHub){
		if(voxlVisionHub['result']){
			healthCheckWritelnPass('           : running: ' + voxlVisionHub['running'])
			//healthCheckWritelnPass('           : mpaPipe: ' + voxlVisionHub['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlVisionHub['running'])
			//healthCheckWritelnFail('           : mpaPipe: ' + voxlVisionHub['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-vision-hub info')
	}
	

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-flow-server -------------------------------------')

	// show high level
	var voxlFlowServer = data['voxl-flow-server'];
	healthCheckWriteln(' < SDK: voxl-flow-server: ')
	if(voxlFlowServer){
		var result = voxlFlowServer['result'];
		console.log("test " + result)
		if(result == true){
			healthCheckWritelnPass('           : running: ' + voxlFlowServer['running'])
			healthCheckWritelnPass('           : mpaPipe: ' + voxlFlowServer['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlFlowServer['running'])
			healthCheckWritelnFail('           : mpaPipe: ' + voxlFlowServer['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to getvoxl-flow-server info')
	}

	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-feature-tracker ---------------------------------')

	// show high level
	var voxlFeatureTracker = data['voxl-feature-tracker'];
	healthCheckWriteln(' < SDK: voxl-feature-tracker: ')
	if(voxlFeatureTracker){
		if(voxlFeatureTracker['result']){
			healthCheckWritelnPass('           : running: ' + voxlFeatureTracker['running'])
			healthCheckWritelnPass('           : mpaPipe: ' + voxlFeatureTracker['mpaPipe'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlFeatureTracker['running'])
			healthCheckWritelnFail('           : mpaPipe: ' + voxlFeatureTracker['mpaPipe'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-feature-tracker info')
	}
	
	healthCheckWriteln('')
	healthCheckWriteln('---- voxl-uvc-server --------------------------------------')

	// show high level
	var voxlUvcServer = data['voxl-uvc-server'];
	healthCheckWriteln(' < SDK: voxl-uvc-server: ')
	if(voxlUvcServer){
		if(voxlUvcServer['result']){
			healthCheckWritelnPass('           : running: ' + voxlUvcServer['running'])
		} else {
			healthCheckWritelnFail('           : running: ' + voxlUvcServer['running'])
		}
	}
	else {
		healthCheckWritelnFail('FATAL ERROR: unable to get voxl-uvc-server info')
	}
}

healthCheckPrintResults = function(data){
	if(data['factoryMode']) {
		if (data['factoryMode'].includes("D0005")){
			healthCheckPrintResultsD0005(data)
		}
		else if (data['factoryMode'].includes("D0006")){
			healthCheckPrintResultsD0006(data)
		}
		else if (data['factoryMode'].includes("D0008")){
			healthCheckPrintResultsD0008(data)
		}
	}
}

writeHeader();

healthChecks = function(factoryMode) {
	healthCheckWriteln("")
	healthCheckWriteln('>> Starting health check for ' + factoryMode);

	$.ajax({
		url: 'api/v1/platform/health-check',
		headers: { 'Content-Type': 'application/json' },
		success : function (data) {
			healthCheckPrintResults(data);
		},
		fail : function(xhr,status,error){
			healthCheckWriteln('XX ERROR: ' + error);
			healthCheckWriteln('XX status: ' + status);
		}
	});
}


$(document).ready(function() { 
	healthCheckWriteln("")
	healthCheckWriteln('Detecting platform type:');
	healthCheckWriteln('>> api/v1/platform/status.json');
	
	$.ajax({
		url: 'api/v1/platform/status.json',
		headers: { 'Content-Type': 'application/json' },
		success : function (data) {
			var result = JSON.stringify(data);

			if(data['factory-mode']){
				healthCheckWriteln('<< ' + data['factory-mode']);
				healthChecks(data['factory-mode'])
			}
			else {
				healthCheckWriteln('XX ERROR:  Unable to get platform status.');
			}
		},
		fail : function(xhr,status,error){
			healthCheckWriteln('XX ERROR: ' + error);
			healthCheckWriteln('XX status: ' + status);
		}
	});
});



term.onKey(e => {
	console.log(e);
	switch (e.key) {
		case '\u0003': // Ctrl+C
			break;

		case '\r': // Enter
			command = '';
			break;

		case '\u007F': // Backspace (DEL)
			break;

		default: // Print all other characters for demo
			term.write(e);
			break;
	}
});
